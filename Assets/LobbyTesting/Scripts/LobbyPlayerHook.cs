﻿using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;
using LobbyPlayer = Prototype.NetworkLobby.LobbyPlayer;

public class LobbyPlayerHook : LobbyHook {
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer) {
        if (lobbyPlayer == null) return;

        LobbyPlayer playerFromLobby = lobbyPlayer.GetComponent<LobbyPlayer>();

        if (playerFromLobby == null) return;
        Debug.Log("<color=yellow>" + this + " - (Server?)Adding player" + gamePlayer + " for lobbyPlayer: " + playerFromLobby.playerName + "</color>");
        GameManager.AddPlayer(gamePlayer, playerFromLobby.slot, playerFromLobby.playerColor, playerFromLobby.playerName, playerFromLobby.playerControllerId);
    }
}