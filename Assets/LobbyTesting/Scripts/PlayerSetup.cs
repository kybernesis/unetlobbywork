﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Cameras;

public class PlayerSetup : NetworkBehaviour {
    public GameObject avatarPrefab;
    public Transform cameraTarget;
    public List<Behaviour> componentsToDisable = new List<Behaviour>();
    public List<GameObject> playerAvatars = new List<GameObject>();

    [SyncVar(hook = "OnSetAvatarSync")]
    public GameObject playerAvatar;

    [SyncVar]
    public Color playerColor;

    [SyncVar]
    public string playerName;

    [SyncVar]
    public int playerNumber;

    [SyncVar]
    public int localID;

    public override void OnStartClient() {
        base.OnStartClient();
        if (!isServer) GameManager.AddPlayer(gameObject, playerNumber, playerColor, playerName, localID);
        if (!isLocalPlayer) DisableComponents();
        foreach (GameObject avatar in playerAvatars) {
            avatar.SetActive(false);
        }
    }

    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();
        if (isLocalPlayer) Debug.Log("<color=aqua>" + this + " - NetID: " + netId + "</color>");

        //        transform.position = new Vector3(transform.position.x + Random.Range(-5, 5), transform.position.y + 1f, transform.position.z);
        gameObject.name = playerName;
        foreach (Behaviour component in componentsToDisable) {
            component.enabled = true;
        }
        //        if (isLocalPlayer) SetAvatar();
    }

    private void Start() {
        Debug.Log("<color=magenta>" + this + " - Start happens!" + "</color>");
        SetAvatar();
    }

    private void DisableComponents() {
        Debug.Log("<color=orange>" + this + " - Disabling components: " + "</color>");
        foreach (Behaviour component in componentsToDisable) {
            Debug.Log("<color=white>" + this + " - Component: " + component + "</color>");
            component.enabled = false;
        }
    }

    public void Init(string pName, Color pColor, int pNum, int localPlayerID) {
        Debug.Log("<color=yellow>" + this + " - Running Init for " + pName + "</color>");
        playerColor = pColor;
        playerName = pName;
        playerNumber = pNum;
        localID = localPlayerID;
        Debug.Log("<color=yellow>" + this + " - Init done - Player: " + playerName + " - playerColor: " + playerColor +
            "\nPlayerNumber: " + playerNumber + " - localID: " + localID + "</color>");
    }

    [Command]
    private void CmdPlayerSetup() {
        gameObject.name = playerName;
    }

    private void SetAvatar() {
        Debug.Log("<color=orange>" + this + " - (Local)setting avatar" + "</color>");
        GameObject avatar = playerAvatars[playerNumber];
        avatar.SetActive(true);
        avatar.GetComponent<Renderer>().material.color = playerColor;
        CmdSetAvatar(avatar);
    }

    [Command]
    private void CmdSetAvatar(GameObject avatar) {
        playerAvatar = avatar;
        //        GameObject avatar = Instantiate(avatarPrefab, transform.position, transform.rotation);
        //        avatar.name = avatarPrefab.name;
        //        avatar.transform.SetParent(transform);
        //
        //        PlayerAvatar playerAvatarComponent = avatar.GetComponent<PlayerAvatar>();
        //        playerAvatarComponent.parentNetID = netId;
        //        playerAvatarComponent.playerColor = playerColor;
        //
        //        Debug.Log("<color=orange>" + this + " - (Cmd)Spawning avatar: " + avatar + "</color>");
        //        NetworkServer.SpawnWithClientAuthority(avatar, connectionToClient);
        //
        //        RpcSetAvatar(avatar);
        //        playerAvatar = avatar;
    }

    //    [ClientRpc]
    //    private void RpcSetAvatar(GameObject avatar) {
    //        if (NetworkServer.active) return;
    //        avatar.transform.SetParent(transform);
    //    }

    private void OnSetAvatarSync(GameObject avatar) {
        Debug.Log("<color=orange>" + this + " - (SyncVar Hook)playerAvatar synced: " + avatar + "</color>");
        playerAvatar = avatar;
        if (!avatar.activeSelf) {
            avatar.SetActive(true);
            avatar.GetComponent<Renderer>().material.color = playerColor;
        }
        //        playerAvatar.transform.parent = transform;
    }
}
