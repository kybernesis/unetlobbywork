﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

internal class GameManager : NetworkBehaviour {
    static public GameManager instance;
    static public List<PlayerManager> players = new List<PlayerManager>();

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    private void Start() {
        DontDestroyOnLoad(gameObject);
    }

    public static void AddPlayer(GameObject player, int playerNum, Color playerColor, string playerName, int localID) {
        PlayerManager newPlayer = new PlayerManager(player, playerNum, playerColor, playerName, localID);
        newPlayer.Setup();

        players.Add(newPlayer);
    }
}