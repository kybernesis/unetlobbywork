﻿using UnityEngine;

internal class PlayerManager {
    private readonly int localPlayerID;
    private readonly GameObject playerInstance;
    private readonly Color playerColor;
    private readonly string playerName;
    private readonly int playerNumber;

    private PlayerSetup playerSetup;

    public PlayerManager(GameObject player, int playerNum, Color playerColor, string playerName, int localID) {
        playerInstance = player;
        playerNumber = playerNum;
        this.playerColor = playerColor;
        this.playerName = playerName;
        localPlayerID = localID;
    }

    public void Setup() {
        Debug.Log("<color=yellow>" + this + " - " + playerName + " added, running setup" + "</color>");
        playerSetup = playerInstance.GetComponent<PlayerSetup>();
        playerSetup.Init(playerName, playerColor, playerNumber, localPlayerID);
    }
}