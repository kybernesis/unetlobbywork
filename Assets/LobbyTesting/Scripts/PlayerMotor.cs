﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerMotor : NetworkBehaviour {
    public float moveSpeed = 12f;
    public float turnSpeed = 180f;

    private float moveInput;
    private float turnInput;
    private Rigidbody playerRigidbody;

    private void Start() {
        playerRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        if (!isLocalPlayer) return;

        moveInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate() {
        if (!isLocalPlayer || playerRigidbody == null) return;

        Move();
        Turn();
    }

    private void Move() {

        Vector3 movement = transform.forward * moveInput * moveSpeed * Time.deltaTime;

        playerRigidbody.MovePosition(playerRigidbody.position + movement);
    }

    private void Turn() {
        float turn = turnInput * turnSpeed * Time.deltaTime;
        Quaternion inputRotation = Quaternion.Euler(0f, turn, 0f);
        playerRigidbody.MoveRotation(playerRigidbody.rotation * inputRotation);
    }
}
