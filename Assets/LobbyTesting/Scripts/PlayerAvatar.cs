﻿using UnityEngine;
using UnityEngine.Networking;

internal class PlayerAvatar : NetworkBehaviour {
    [SyncVar]
    public NetworkInstanceId parentNetID;

    [SyncVar]
    public Color playerColor;

    public override void OnStartAuthority() {
        base.OnStartAuthority();
        Debug.Log("<color=aqua>" + this + " - NetID: " + netId + " - parentNetID: " + parentNetID + "</color>");
        GameObject parentObject = ClientScene.FindLocalObject(parentNetID);
        transform.SetParent(parentObject.transform);
        GetComponent<Renderer>().material.color = playerColor;
    }
}